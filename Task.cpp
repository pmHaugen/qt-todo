#include "Task.h"
#include "ui_Task.h"

#include <QInputDialog>
#include <QDebug>

Task::Task(const QString& name, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Task)
{
    ui->setupUi(this);
    setName(name);

    connect(ui->editButton, &QPushButton::clicked, this, &Task::rename);
    connect(ui->removeButton, &QPushButton::clicked, [this] {
        emit removed(this);
    });
    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);
    connect(ui->descriptionButton, &QPushButton::clicked, this, &Task::setDescription);
    setDescriptionText("");
}

Task::~Task()
{
    qDebug() << "~Task() called";
    delete ui;
}

void Task::setName(const QString& name)
{
    ui->checkbox->setText(name);
}
void Task::setDescriptionText(const QString& desc)
{
QFont font = ui->descriptionBox->font();

    if (!desc.isEmpty())
    {
        font.setBold(true);
        font.setItalic(false);
        ui->descriptionBox->setFont(font);
        ui->descriptionBox->setText(desc);
    }
    else
    {

        font.setItalic(true);
        font.setBold(false);
        ui->descriptionBox->setText("no description");
        ui->descriptionBox->setFont(font);
    }
}
QString Task::name() const
{
    return ui->checkbox->text();
}

bool Task::isCompleted() const
{
    return ui->checkbox->isChecked();
}

void Task::rename()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Edit task"),
                                          tr("Task name"), QLineEdit::Normal,
                                          this->name(), &ok);
    if (ok && !value.isEmpty()) {
        setName(value);
    }
}

void Task::checked(bool checked)
{
    QFont font(ui->checkbox->font());
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);

    emit statusChanged(this);
}
void Task::setDescription()
{
    bool ok;
    QString desc = QInputDialog::getText(this, tr("Add Description"),
                                         tr("Description:"), QLineEdit::Normal,
                                         ui->descriptionBox->text(), &ok);
    if(ok)
        setDescriptionText(desc);

}

